---
title: Actors
date: 2023-12-21 10:01:01
---

Below you will find a list of actors in the open source mobile ecosystem.

# Alternative mobile OS
## Projects still maintained

| Name | Description | URL | Supports running Android apps| Supported commmunication networks |
| --- | --- | --- | --- | --- |
| /e/OS | AOSP deGoogled mobile OS | https://murena.com | Yes | up to 5G |
| ExpidusOS | A cross platform OS | https://expidusos.com | No | None |
| FuriOS | Debian based mobile OS | https://furilabs.com/ | Yes |  up to 4G/VoLTE ||
| GrapheneOS | AOSP based OS with focus on privacy and security | https://grapheneos.org/ | Yes | up to 5G |
| iodéOS | AOSP deGoogled mobile OS | https://iode.tech/iodeos/ | Yes | up to 5G |
| LineageOS | AOSP based mobile OS | https://lineageos.org/ | Yes | up to 5G |
| Mobian | Debian based mobile OS | https://mobian-project.org/ | No | up to 4G/VoLTE |
| Nemo Mobile | Manajaro Linux based OS | https://nemomobile.net/ | - | up to 4G |
| postmarketOS | Alpine Linux based mobile OS | https://postmarketos.org/ | Yes | up to 4G (no VoLTE) |
| PureOS | Debian based mobile OS | https://puri.sm/ | No | up to 4G/VoLTE |
| KaiOS | Firefox OS based OS for non-touch smartphones | https://www.kaiostech.com/ | No | up to 4G/LTE |
| Replicant | Fully free AOSP based OS | https://www.replicant.us/ | Yes | up to 3G |
| Sailfish OS | MeeGo based mobile OS | https://sailfishos.org/ | Yes | up to 5G |
| Sculpt OS | Open source general-pupose OS with an experimental phone variant for the PinePhone | https://genode.org/index | No | - |
| Tizen | Flexible OS with multiple profiles for different industries | https://www.tizen.org/ | - | Up to 4G/LTE |
| Ubuntu Touch | Ubuntu based mobile OS| https://ubports.com | No | up to 4G/VoLTE |
| WebOS Ports | Open webOS based mobile OS | https://webos-ports.org | No | - |

#### "-" = no information could be found.
#### What's the difference between Android Open Source Project (AOSP) and Android ? AOSP is Android without most of the Google Mobile Services (GMS), such as the Google Play Store. AOSP based mobile OSes are similar to common Android phones. AOSP is developped by Alphabet (Google), and developers can modify it to their likings.

# Phone manufacturers 
## Companies selling phones with alternative mobile OSes

| Name | URL | Available pre-installed OSes |
| --- | --- | --- |
| Furi Labs | https://furilabs.com/ | FuryOS |
| Murena | https://murena.com | /e/OS |
| Pine64 | https://pine64.org/, https://pine64eu.com/ | postmarketOS, Mobian, Manjaro+Plasma Mobile |
| Purism | https://puri.sm/ | PureOS |
| Volla | https://volla.online | Ubuntu Touch, Volla OS |
| Jolla/Reeder | https://jolla.com/ | Sailfish OS |

#### Why is X company not listed ? Some companies phones' are compatible with previously listed OSes, but only sell their phones with Android, ie: Fairphone.

# Associations promoting open source mobile software

| Name | Description | URL | Country of origin |
| --- | --- | --- | --- |
| AFUL | Promoting open software and use of open standards. | https://aful.org/ | France |
| April | Promote and defend open software. | https://april.org | France |

Do you think something is missing or imprecise ? Write us at contact[at]sailmates.net 
