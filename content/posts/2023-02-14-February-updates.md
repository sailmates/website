---
title: February 2023 - Announcement
date: 2023-06-21
categories: verein
keywords:
---

Preparations continue for going public.

Email in the form of aliases such as `contact`(at)`sailmates.net` is set up by the Institute for Common Good Technology ("ICGT").

Hosting of `sailmates.net` and deployment of the main branch from the [codeberg website repo](https://codeberg.org/sailmates/website) is also set up by ICGT.

`poetaster` sets up the Matrix instance for `sailmates.net`.

Also, `jojo` makes Assoconnect and Liberapay (donations only) available, in addition to wire transfer.


On February 14, 2023, we officially presented the association in the Sailfish OS [Forums](https://forum.sailfishos.org/t/join-the-sailmates-association/14627).

The association had 6 members when officially announced. 
In the remaining days of February, we welcomed 11 more members.


<!--
	raw notes
1 Feb First forwarding aliases such as **chair at sailmates.net** Institute for Common Good Technology (ICGT)
1 Feb Hosting of **sailmates.net** moved to Institute for Common Good Technology servers from codeberg pages.
6 Feb Assoconnect can be used for membership / donations in addition to wire transfer
7 Feb sailmates.net has a matrix instance now, thanks to @poetaster
8 Feb Website deployment from https://codeberg.org/sailmates/website main branch are now done on ICGT servers
10 Feb Liberapay can be used for donations (not membership fees)
14 Feb The official announcement of opening membership for all on Sailfish [Forums](https://forum.sailfishos.org/t/join-the-sailmates-association/14627)
27 Feb The association has 6 members when announced officially. In the next days remaining in February we became 17
-->