---
title: April 2024 - New nlnet grant NGI Mobifree
date: 2024-04-03 00:00:00
categories: verein
keywords:
---

[Mobifree](https://mobifree.org/) is a consortium within the Next Generation Internet initiative. Their
second call for the NGI Mobifree grant opened in the beginning of April and the
deadline is the first of June of this year.

Its eligible for everyone, as stated in the [website](https://nlnet.nl/mobifree/eligibility/index.html): 'There are no categorical
exclusions of persons who may not receive support from NGI Mobifree.'

A long list of activity types qualify for the grant. Here is a list of examples,
provided by nlnet, on past projects that got the support:

- /e/OS 
- decentralized app stores (such as F-Droid)
- pro-privacy mobile apps regarding communications, maps and emergency response
- end to end encryption mechanisms

The proposals have to be between 5.000 and 50.000 euros, with a hard limit of
60.000 euros.

Links: 

- [https://mobifree.org/](https://mobifree.org/)
- [https://nlnet.nl/mobifree/](https://nlnet.nl/mobifree/)
