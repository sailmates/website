---
title: December 2023 - Open source smartphones at 37c3
date: 2024-01-08 13:00:54
categories: verein
keywords:
---

Last December, during 37c3, three events were held on the subject of open source smartphones.

The first two focused on Replicant, an Android distribution certified by the Free Software Foundation. They included an introduction to this OS, but also difficulties and future developments that will take place on Replicant (such as a PinePhone port).

The third session looked at the "state of smartphone freedom in 2023", and as you can see from the table below, the hardware options are few and far between:

![](./Smartphones_freedom_status_2023-1.png)
![](./Smartphones_freedom_status_2023-2.png)

The Sailmates association wasn't able to attend, but you can find the links to
the presentation on the Replicant wiki:

[https://redmine.replicant.us/projects/replicant/wiki/Presentations#37C3](https://redmine.replicant.us/projects/replicant/wiki/Presentations#37C3)
