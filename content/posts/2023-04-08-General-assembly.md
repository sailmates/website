---
title: April 2023 - General Assembly
date: 2023-06-16 16:56:54
categories: verein
keywords:
---

The first General Assembly meeting is held on 8th April.

In the online meeting, the members discussed the financial status and possible future projects for which we want to find funding.
The notes are available by email to all 20 members.

Meanwhile, Mark's first funding application for keypeer.org did not get accepted for nlnet.nl funding.

