---
title: Statutes
date: 2022-12-10 16:56:54
categories: verein
keywords:
---

Below you can find the association's statutes, the original and official version is the one in german:

[Statuten_des_Verein_Sailmates.pdf - Official](Statuten_des_Verein_Sailmates.pdf)

[Translated version using DeepL.com - Unofficial](Statuten_des_Verein_Sailmates_en-GB.pdf)
