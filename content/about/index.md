---
title: About
date: 2022-12-08 22:00:00
---

Sailmates is a non-profit association aiming to support and promote free and open source mobile operating system alternatives.

You can contact us through: contact[at]sailmates.net

The board members, the association's address, and the legal registration of the association can be found on the Austrian association registry website: https://citizen.bmi.gv.at/at.gv.bmi.fnsweb-p/zvn/public/Registerauszug , our ZVR-Zahl (association number) is 1967552494.

Below you will find the minutes of the general assemblies of the association:

2023 General Assembly:
* [GA-minutes](./2023-04-17/2023-04-17_GA.txt)
* [GA-prentation](./2023-04-17/sailmates_presentation.odp)

2024 General Assembly:
* [GA-minutes](./2024-10-02/2024_10_02.txt)
* [GA-presentation](./2024-10-02/sailmates_ga_2024.odp)
