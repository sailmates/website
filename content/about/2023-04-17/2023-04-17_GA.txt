**__General Assembly of the Sailmates Association__**

**Participants**:

* member's names have been replaced by stars (*), except for Jonathan.

**Meeting Date**: 17.04.2023

**Start of the Meeting**: 19:06 (CET)

**Agenda:**

* Budget review.
* Elections of two auditors. 
* Adoption of a resolution regarding the first project(s) that will be funded with the association's budget
* Questions

**MoM:**

Jonathan welcomes the participants

Budget review:

* Jonathan gives an overview over the financial situation. The details can be found in the Presentation. Jonathan explained the details of the current finance situation and the state of the * membership.

Elections of two auditors:

* Jonathan explained the Tasks and the Liability of the auditors.
* * will volunteer if no other person is volunteering for the auditor.
* * is volunteering for the auditor.
* Jonathan starts a voting
* * (y/n/a): 7/0/1
* * (y/n/a): 7/0/1
* * and * have been voted as auditors

Project suggested

* Jonathan is showing the different proposed project to be potentially funded. All of the details can be found in the Jonathan's presentation.
* Camera2 API: We have Members (* and * at least) that are willing to help testing. Jonathan is proposing to wait, until more information is available on that. We will offer mal to help.
* Nextcloud Hosting for Members: Nextcloud for Members, * has some "underutilised" hosting and could offer that. The question would be if we want to continue and potentially follow that idea.
* Patch Manager: Relatively expensive hosting at the Moment. Basil is requesting some donations probably. To vote: if we want to support Basil technically and financially.
* Gitlab CI for porters: obs is used to build apps (via chum) and general packages for ports. The flashing images are although build at gitlab ci. The problem: Jolla might shut down obs. They thought about shutting it down already once. General conclusion: continue with getting some interview and additional information from pig/adam and porters
* getting push notification working on SFOS: find out about the real need to spend some efforts on this, create a working group. check about privacy, investigate more into this. How should run the server? Several scenarios possible.
* improve telepathy: abstraction layer to unify chat protocols. revisit it later, find out a little more on codeberg
* sponsoring for university projects: take a look at the presentation. gather phones to sponsor some? three options: financial support, support with phones, organise gathering of old devices? some kind of summer camp?
* Build a hosting working group? What could be hosted?

Polls on Financing:

* Support Basil financially for patchmanager and openrepos. (y/n/a): 7/0/1 (*: a)
* Support Basil on a plan to host both in the future by sailmates. (y/n/a): 7/0/1 (*: y)
* Build a Hosting working group to bring up ideas what would be possible to host "in-house" and what it would cost. If the Idea is financially feasible the Board can decide to go with that. (y/n/a): (8/0/0) (*: y)
* create a working group for debate and investigate about push notification services on sailfish os. (y/n/a): (6/0/0) (*, * left already) 
* Should we provide technical and financial help for university projects (such as hosting services or gathering old hardware from forum members). (y/n/a): (7/0/0)

 

General Discussion

* *: update on keypeer: need more API ideas
* *: workflow on codeberg: add cards and organise already discussed items, especially for keypeer
* hosting: solve everything with second access possibility. nothing should depend on one person
