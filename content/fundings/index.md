---
title: Fundings
date: 2023-03-08 10:00:00
---

Bellow you will find a list of funding programs suitable for open source,
mobile environments.

|Name|Website|Restriction|
|:---|:---|:---|
|Prototype Fund|https://prototypefund.de|German tax payer only|
|Open Technology Fund|https://www.opentech.fund||
|netidee|https://www.netidee.at|Austrian address|
|SIDN Fonds|https://www.sidnfonds.nl|Primary impact in the Netherlands|
|Sovereign Tech Fund|https://sovereigntechfund.de|Technologies must be under-supplied or threatened by other circumstances|
|Mobifree|https://mobifree.org|-|
|Futo|https://www.futo.org/grants/|-|
|Floss/fund|https://floss.fund/|-|
|FOSS United|https://fossunited.org/|-|
|GitHub Secure Open Source Fund|https://resources.github.com/github-secure-open-source-fund/|Be on GitHub, improve security, located in regions supported by GitHubSponsors|

## Prototype Fund
### Requirements
* Application must be submitted in German.
* Applicant must be a natural person. 
* Applicant must live and pay taxes in Germany.
* Project has to be implemented within 6 months.
* Project can't be double funded.

### Links
FAQ - https://prototypefund.de/en/apply/faq/

## Open Technology Fund
### Requirements
* No specific requirements except that it matches the OTF's vision:
	* Access to the Internet, including tools to circumvent website blocks, connection blackouts, and widespread censorship;
	* Awareness of access, privacy, or security threats and protective measures, including how-to guides, instructional apps, data collection platforms, and other efforts that increase the efficacy of Internet freedom tools;
	* Privacy enhancement, including the ability to be free from repressive observation and the option to be anonymous when accessing the Internet; 
	* Security from danger or threat when accessing the Internet, including encryption tools.
* Application should be submitted in English (not a must)
* Also suitable for fellowships

### Links
FAQ - https://guide.opentech.fund/faq

## netidee
### Requirements
* Natural persons/associations with an Austrian residential address
* Preferably apply in German but can be in English too (the website/documents
* are in German only)

### Links
FAQ - https://www.netidee.at/faq
Project documents page - https://www.netidee.at/einreichen/projekt

## SIDN Fonds
### Requirements
* Project has a primary impact in the Netherlands
* Project did not start yet
* Project is shorter than 6 months
* Can be done by natural persons or organisations

### Links
FAQ - https://www.sidnfonds.nl/faq

## Sovereign Tech Fund
### Requirements
* Not a user facing Application
* Societal relevance
* Needs a funding of at least 50k€
* Tech must be under-supplied or threatened by other circumstances

### Example of financed projects
* Wireguard
* curl
* Fortran package manager
* Improving JS ecosystem, infrastructure and security

### Links
Kinda FAQ - https://sovereigntechfund.de/en/applications/

## Mobifree
### Requirements
Mobifree is part of the nlnet foundation and a consortium with other companies and institutes.

Project needs to meet at least one of these topics:

- scientific research
- design and development of free and open source software and open hardware
- validation or constructive inquiry into existing or novel technical solutions
- software engineering aimed at adapting to new usage areas or improving software quality
- formal security proofs, security audits, setup and design of software testing and continuous integration
- documentation for researchers, developers and end users, including educational materials
- standardisation activities, including membership fees of standards bodies
- understanding user requirements and improving usability/inclusive design
- necessary measures in support of (broad)er deployability, e.g. packaging
- participation in technical, developer and community events like hackathons, IETF, W3C, RIPE meetings, FOSDEM, etc. (admission fee, travel and subsistence costs)
- other activities that are relevant to adhering to robust software development and deployment practices
- project management
- out-of-pocket costs for infrastructure essential to achieving the above

No categorical exclusions of persons, inhabitants of the EU and Horizon Europe associted countries are given priority.
### Example of financed projects

Since this is fairly recent there are no financed projects already disclosed.

### Links
https://nlnet.nl/mobifree/eligibility/

## Futo
### Requirements
No requirements specified.

### Links
https://www.futo.org/grants/

## Floss/fund
### Requirements
No requirements specified.

### Links
https://floss.fund/blog/announcing-floss-fund/

## FOSS United
### Requirements
No requirements specified.

### Links
https://fossunited.org/

## GitHub Secure Open Source Fund
### Requirements
- Current maintainer of an open source project
- Be age eighteen (18) or older
- Have an active online profile on GitHub
- Be located in one of the regions supported by GitHub Sponsors
- Not be a current employee of GitHub and/or any of its parent/subsidiary companies
- Clear open source license
- Open source first project with demonstrated community traction and adoption
- Clear governance structure prior to kick-off
- Interest and willingness to to engage and improve security
- Commitment from core leaders to participate in and engage in the required programming
- Agree to Code of Conduct and Privacy Statement

### Links
https://resources.github.com/github-secure-open-source-fund/

### Links
https://resources.github.com/github-secure-open-source-fund/
