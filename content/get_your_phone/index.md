---
title: About
date: 2024-11-30 00:00:00
---

## How it works ?

At Sailmates, our mission is to help people explore and adopt alternative mobile operating systems. For those who may not have the technical expertise to flash a new OS themselves, we offer pre-flashed phones with the operating system of your choice.

Since this isn't a "click and buy" process, here's how it works:

1. Contact us by mail: contact[at]sailmates.net
2. Specify the OS you'd like to have installed.

Once we receive your request, we'll reply with a list of compatible devices and pricing, so you can select the option that suits you best. All phones are second-hand. We do not make a profit on the phones themselves; we sell them at the same price we paid to acquire them, with no markup.

## Which OSes are available ?

- Mobian
- postmarketOS
- Sailfish OS
- Ubuntu touch
